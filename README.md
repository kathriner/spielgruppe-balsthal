## Spielgruppe Balsthal
Webseite der Spielgruppe Balsthal

## Code style

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat)](https://github.com/feross/standard)
 
## Tech/framework used

<b>Built with</b>

- [DreamViewer](https://www.adobe.com/ch_de/products/dreamweaver.html)

- [InteliJ](https://www.jetbrains.com/idea/)


## Installation
Start the .html files from your Finder or Explorer


## Credits
Give proper credits. This could be a link to any repo which inspired you to build this project, any blogposts or links to people who contrbuted in this project. 

#### Anything else that seems useful

## License
MIT ©

## Developers
- [Roman Kathriner](http://romankathriner.rf.gd/)
